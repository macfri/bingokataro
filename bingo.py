

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Bingo.py: Bingo Kata"""

__author__      = "Ro Martinez"
__copyright__   = "Copyright 2020, Bingo Kata"


import random
import pprint



def calling_bingo_numbers_us():
	BINGO_RANGE_START = 1
	BINGO_RANGE_END = 75
	numbers = []
	while True:
		n = random.randint(BINGO_RANGE_START, BINGO_RANGE_END)
		if n not in numbers:
			numbers.append(n)
		if len(numbers) == BINGO_RANGE_END:
			break
	return numbers


def generate_bingo_cards_us(players):
	BINGO_CARD_ROWS = 5
	BINGO_CARD_COLS = 5
	cards = []
	for ncards in range(0, players):
		arr_card = []
		for x in range(BINGO_CARD_ROWS):
			arr_tmp = []
			z = 1
			for y in range(BINGO_CARD_COLS):
				n = random.randint(z, z + 14)
				z = z + 15
				if x == 2 and y == 2:
					n = " "
				arr_tmp.append(n)
			arr_card.append(arr_tmp)
		cards.append(arr_card)
	return cards



def checking_bingo_cards_us(numbers, cards):
	BINGO_CHECK_FULL_CARD = 24
	results = {}
	_continue = True
	for card_number, random_number in enumerate(numbers):
		if _continue:
			for cardId, card in enumerate(cards):
				check = 0
				for rowt in card:
					for c in rowt:
						if c == random_number:
							check = check + 1			
				if cardId not in results:
					results[cardId] = check
				else:
					results[cardId] = results[cardId] + check
				if results[cardId] == BINGO_CHECK_FULL_CARD:				
					_continue  = False
					break
	return results.values()



if __name__ == "__main__":

	pp = pprint.PrettyPrinter(indent=4)

	BINGO_PLAYERS = int(input("# players: "))	
	BINGO_NUMBERS = calling_bingo_numbers_us()
	BINGO_CARDS = generate_bingo_cards_us(BINGO_PLAYERS)
	BINGO_RESULTS = checking_bingo_cards_us(BINGO_NUMBERS, BINGO_CARDS)
	BINGO_WINNER_NUMBER = 24

	print("CALLING BINGO NUMBERS:")
	print("-" * 100)
	print(BINGO_NUMBERS)	

	print(" ")

	print("GENERATING BINGO CARDS:")
	print("-" * 100)

	for cardId, card in enumerate(BINGO_CARDS):

		print("Player %d " % (cardId + 1) )
		print(" ")
		pp.pprint(card)
		print(" ")


	print(" ")

	print("CHECKING BINGO CARDS:")
	print("-" * 100)
	print("Results: %s" %  BINGO_RESULTS)
	for i, x in enumerate(BINGO_RESULTS):
		if x == BINGO_WINNER_NUMBER:
			print("BINGO: Player %d is winner " % (i + 1))

	print(" ")


