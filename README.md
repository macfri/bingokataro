# Bingo Kata

Bingo is a game of chance played using cards with numbers printed on them, which are marked off of the card as the caller reads out some manner of random numbers. Play usually ceases once a certain pattern is achieved on a card, where the winner will shout out "Bingo!" in order to let the caller and the room know that there may be a winner.
There are lots of different variations of Bingo, each with their own specific rules. Classic US Bingo is perhaps the most well known, where the game is played using a 5x5 grid of numbers between 1 and 75, with a FREE space in the middle. There is also a UK version of Bingo, which uses a 9x3 grid of spaces containing numbers between 1 and 90, of which five spaces on each row are filled in.


### Prerequisites

- Python 2.7
- optional: pep8 - Python style guide checker

## Running the tests

Run bingo.py in the console and it prompts you how many players you want, 
it will you the numbers of cards and who wins

it show three parts

	CALLING BINGO NUMBERS

	GENERATING BINGO CARDS

	CHECKING BINGO CARDS


### Big O Notation

- calling_bingo_numbers_us

	O(N)

- generate_bingo_cards_us()\

	O(10) + O(5) + O(5) = O(N “ 3)

- checking_bingo_cards_us(numbers, cards) \

	O(75) + O(N) + O(N) + O(N) = O(N “ 4)


## Contributing

- Ronald Martinez Iglesias <macfri10@gmail.com>

## Authors

* **Ronald Martinez** - *Initial work* - [BingoKataRo](https://bitbucket.org/macfri/bingokataro/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
